'''
compiles .less files from ./src directory to css files in ./dst directory
'''
from datetime import datetime

import os, sys
from collections import OrderedDict
from itertools import chain
from multiprocessing import Pool


base_command = 'lessc ./src/{src} ./dst/{dst} {flags}'
flags = OrderedDict((
    ('none', ''),
    ('autoprefix', ' --autoprefix="defaults"')
  ))


def compile_css(formaters):
    formaters['dst'] = formaters['src'].replace('.less', '.css')
    os.system(base_command.format(**formaters))
    return formaters['src']


if __name__ == '__main__':
  start_time = datetime.now()
  print('start time:', start_time.strftime('%I:%M:%S'))  # print time

  _, changed_file = sys.argv
  changed_file_basename = os.path.basename(changed_file)

  less_files = tuple(chain(
      (changed_file_basename,),
      filter(
          lambda name: name.endswith('.less') and name != changed_file_basename,
          os.listdir('src')
        ),
    ))

  src_dst_flags = (dict(src=lf, flags=ap)
                   for ap in flags.values() for lf in less_files)

  with Pool() as pool:
      for file_name in pool.imap_unordered(compile_css, src_dst_flags):
          print(file_name, end=' ')

      print()


  end_time = datetime.now()
  run_time = end_time - start_time
  print('run_time:', run_time.total_seconds(), 'seconds', end='\n' * 2)
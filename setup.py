from setuptools import setup

setup(name='less2css',
      version='0.0.0',
      license='MIT',
      description='Converts all .less files in ./src directory to .css files in ./dst directory.',
      author='William Rusnack',
      author_email='williamrusnack@gmail.com',
      classifiers=['Development Status :: 2 - Pre-Alpha', 'Programming Language :: Python :: 3'],
      py_modules=['less2css'],
      install_requires=['when-changed'],
     )

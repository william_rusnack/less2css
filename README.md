# less2css
Converts all .less files in ./src directory to .css files in ./dst directory.

## Install
```shell
npm install -g less less-plugin-autoprefix
pip install git+https://github.com/BebeSparkelSparkel/less2css.git
```

## How to use?
Create a `./src/` directory and move all your .less files into it.  
Then create a `./dst/` directory for the generated .css files to be moved into.  
Generate the css files.  
Watch files for change.  
```shell
mkdir src  # holds the .less source files
mv ./*.less ./src  # moves .less files into the src directory

mkdir dst  # holds the .css generated files

python -m less2css  # Generate the css files
when-changed src/ -c "python -m less2css"  # Watch files for change
```
